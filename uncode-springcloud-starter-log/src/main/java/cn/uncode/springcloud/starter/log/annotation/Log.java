package cn.uncode.springcloud.starter.log.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 权限注解 用于检查权限 规定访问权限
 *
 * @author Juny
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface Log {

	/**
	 * 日志内容<br/>
	 * 示例：修改用户信息，用户ID为：{in:userId},用户名称修改为：{out:user[1].name}
	 */
	String value();
    /**
     * 模块名称<br/>
     * 如：用户中心-角色管理
     */
    String model() default "";
    /**
     * 操作资源名称<br/>
     * 如：用户
     */
    String target() default "";
    /**
     * 操作类型
     */
    OperationType type() default OperationType.SEARCH;

}

