package cn.uncode.springcloud.utils.net;


import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RestTemplateUtil {
	
	private static final RestTemplate TEMPLATE = new RestTemplate();

    // ----------------------------------GET-------------------------------------------------------
    /**
     * GET请求调用方式
     * 
     * @param url 请求URL
     * @param responseType 返回对象类型
     * @return ResponseEntity 响应对象封装类
     */
    public static <T> ResponseEntity<T> get(String url, Class<T> responseType) {
    	try {
    		return TEMPLATE.getForEntity(url, responseType);
		} catch (Exception e) {
			log.info("get error", e);
		}
        return null;
    }

    /**
     * GET请求调用方式
     * 
     * @param url 请求URL
     * @param responseType 返回对象类型
     * @param uriVariables URL中的变量，按顺序依次对应
     * @return ResponseEntity 响应对象封装类
     */
    public static <T> ResponseEntity<T> get(String url, Class<T> responseType, Object... uriVariables) {
        try {
        	return TEMPLATE.getForEntity(url, responseType, uriVariables);
		} catch (Exception e) {
			log.info("get error", e);
		}
        return null;
    }

    /**
     * GET请求调用方式
     * 
     * @param url 请求URL
     * @param responseType 返回对象类型
     * @param uriVariables URL中的变量，与Map中的key对应
     * @return ResponseEntity 响应对象封装类
     */
    public static <T> ResponseEntity<T> get(String url, Class<T> responseType, Map<String, ?> uriVariables) {
        try {
        	return TEMPLATE.getForEntity(url, responseType, uriVariables);
		} catch (Exception e) {
			log.info("get error", e);
		}
        return null;
    }

    /**
     * 带请求头的GET请求调用方式
     * 
     * @param url 请求URL
     * @param headers 请求头参数
     * @param responseType 返回对象类型
     * @param uriVariables URL中的变量，按顺序依次对应
     * @return ResponseEntity 响应对象封装类
     */
    public static <T> ResponseEntity<T> get(String url, Map<String, String> headers, Class<T> responseType, Object... uriVariables) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAll(headers);
        try {
        	return get(url, httpHeaders, responseType, uriVariables);
		} catch (Exception e) {
			log.info("get error", e);
		}
        return null;
    }

    /**
     * 带请求头的GET请求调用方式
     * 
     * @param url 请求URL
     * @param headers 请求头参数
     * @param responseType 返回对象类型
     * @param uriVariables URL中的变量，按顺序依次对应
     * @return ResponseEntity 响应对象封装类
     */
    public static <T> ResponseEntity<T> get(String url, HttpHeaders headers, Class<T> responseType, Object... uriVariables) {
        HttpEntity<?> requestEntity = new HttpEntity<>(headers);
        try {
        	return exchange(url, HttpMethod.GET, requestEntity, responseType, uriVariables);
		} catch (Exception e) {
			log.info("get error", e);
		}
        return null;
    }

    /**
     * 带请求头的GET请求调用方式
     * 
     * @param url 请求URL
     * @param headers 请求头参数
     * @param responseType 返回对象类型
     * @param uriVariables URL中的变量，与Map中的key对应
     * @return ResponseEntity 响应对象封装类
     */
    public static <T> ResponseEntity<T> get(String url, Map<String, String> headers, Class<T> responseType, Map<String, ?> uriVariables) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAll(headers);
        try {
        	return get(url, httpHeaders, responseType, uriVariables);
		} catch (Exception e) {
			log.info("get error", e);
		}
        return null;
    }

    /**
     * 带请求头的GET请求调用方式
     * 
     * @param url 请求URL
     * @param headers 请求头参数
     * @param responseType 返回对象类型
     * @param uriVariables URL中的变量，与Map中的key对应
     * @return ResponseEntity 响应对象封装类
     */
    public static <T> ResponseEntity<T> get(String url, HttpHeaders headers, Class<T> responseType, Map<String, ?> uriVariables) {
        HttpEntity<?> requestEntity = new HttpEntity<>(headers);
        try {
        	return exchange(url, HttpMethod.GET, requestEntity, responseType, uriVariables);
		} catch (Exception e) {
			log.info("get error", e);
		}
        return null;
    }

    // ----------------------------------POST-------------------------------------------------------

    /**
     * POST请求调用方式
     * 
     * @param url 请求URL
     * @param requestBody 请求参数体
     * @param responseType 返回对象类型
     * @return ResponseEntity 响应对象封装类
     */
    public static <T> ResponseEntity<T> post(String url, Object requestBody, Class<T> responseType) {
        try {
        	return TEMPLATE.postForEntity(url, requestBody, responseType);
		} catch (Exception e) {
			log.info("post error", e);
		}
        return null;
    }

    /**
     * POST请求调用方式
     * 
     * @param url 请求URL
     * @param requestBody 请求参数体
     * @param responseType 返回对象类型
     * @param uriVariables URL中的变量，按顺序依次对应
     * @return ResponseEntity 响应对象封装类
     */
    public static <T> ResponseEntity<T> post(String url, Object requestBody, Class<T> responseType, Object... uriVariables) {
        try {
        	return TEMPLATE.postForEntity(url, requestBody, responseType, uriVariables);
		} catch (Exception e) {
			log.info("post error", e);
		}
        return null;
    }

    /**
     * POST请求调用方式
     * 
     * @param url 请求URL
     * @param requestBody 请求参数体
     * @param responseType 返回对象类型
     * @param uriVariables URL中的变量，与Map中的key对应
     * @return ResponseEntity 响应对象封装类
     */
    public static <T> ResponseEntity<T> post(String url, Object requestBody, Class<T> responseType, Map<String, ?> uriVariables) {
        try {
        	return TEMPLATE.postForEntity(url, requestBody, responseType, uriVariables);
		} catch (Exception e) {
			log.info("post error", e);
		}
        return null;
    }

    /**
     * 带请求头的POST请求调用方式
     * 
     * @param url 请求URL
     * @param headers 请求头参数
     * @param requestBody 请求参数体
     * @param responseType 返回对象类型
     * @param uriVariables URL中的变量，按顺序依次对应
     * @return ResponseEntity 响应对象封装类
     */
    public static <T> ResponseEntity<T> post(String url, Map<String, String> headers, Object requestBody, Class<T> responseType, Object... uriVariables) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAll(headers);
        try {
        	return post(url, httpHeaders, requestBody, responseType, uriVariables);
		} catch (Exception e) {
			log.info("post error", e);
		}
        return null;
    }

    /**
     * 带请求头的POST请求调用方式
     * 
     * @param url 请求URL
     * @param headers 请求头参数
     * @param requestBody 请求参数体
     * @param responseType 返回对象类型
     * @param uriVariables URL中的变量，按顺序依次对应
     * @return ResponseEntity 响应对象封装类
     */
    public static <T> ResponseEntity<T> post(String url, HttpHeaders headers, Object requestBody, Class<T> responseType, Object... uriVariables) {
        HttpEntity<Object> requestEntity = new HttpEntity<Object>(requestBody, headers);
        try {
        	return post(url, requestEntity, responseType, uriVariables);
		} catch (Exception e) {
			log.info("post error", e);
		}
        return null;
    }

    /**
     * 带请求头的POST请求调用方式
     * 
     * @param url 请求URL
     * @param headers 请求头参数
     * @param requestBody 请求参数体
     * @param responseType 返回对象类型
     * @param uriVariables URL中的变量，与Map中的key对应
     * @return ResponseEntity 响应对象封装类
     */
    public static <T> ResponseEntity<T> post(String url, Map<String, String> headers, Object requestBody, Class<T> responseType, Map<String, ?> uriVariables) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAll(headers);
        try {
        	return post(url, httpHeaders, requestBody, responseType, uriVariables);
		} catch (Exception e) {
			log.info("post error", e);
		}
        return null;
    }

    /**
     * 带请求头的POST请求调用方式
     * 
     * @param url 请求URL
     * @param headers 请求头参数
     * @param requestBody 请求参数体
     * @param responseType 返回对象类型
     * @param uriVariables URL中的变量，与Map中的key对应
     * @return ResponseEntity 响应对象封装类
     */
    public static <T> ResponseEntity<T> post(String url, HttpHeaders headers, Object requestBody, Class<T> responseType, Map<String, ?> uriVariables) {
        HttpEntity<Object> requestEntity = new HttpEntity<Object>(requestBody, headers);
        try {
        	return post(url, requestEntity, responseType, uriVariables);
		} catch (Exception e) {
			log.info("post error", e);
		}
        return null;
    }

    /**
     * 自定义请求头和请求体的POST请求调用方式
     * 
     * @param url 请求URL
     * @param requestEntity 请求头和请求体封装对象
     * @param responseType 返回对象类型
     * @param uriVariables URL中的变量，按顺序依次对应
     * @return ResponseEntity 响应对象封装类
     */
    public static <T> ResponseEntity<T> post(String url, HttpEntity<?> requestEntity, Class<T> responseType, Object... uriVariables) {
        try {
        	return TEMPLATE.exchange(url, HttpMethod.POST, requestEntity, responseType, uriVariables);
		} catch (Exception e) {
			log.info("post error", e);
		}
        return null;
    }

    /**
     * 自定义请求头和请求体的POST请求调用方式
     * 
     * @param url 请求URL
     * @param requestEntity 请求头和请求体封装对象
     * @param responseType 返回对象类型
     * @param uriVariables URL中的变量，与Map中的key对应
     * @return ResponseEntity 响应对象封装类
     */
    public static <T> ResponseEntity<T> post(String url, HttpEntity<?> requestEntity, Class<T> responseType, Map<String, ?> uriVariables) {
        try {
        	return TEMPLATE.exchange(url, HttpMethod.POST, requestEntity, responseType, uriVariables);
		} catch (Exception e) {
			log.info("post error", e);
		}
        return null;
    }

    // ----------------------------------通用方法-------------------------------------------------------

    /**
     * 通用调用方式
     * 
     * @param url 请求URL
     * @param method 请求方法类型
     * @param requestEntity 请求头和请求体封装对象
     * @param responseType 返回对象类型
     * @param uriVariables URL中的变量，按顺序依次对应
     * @return ResponseEntity 响应对象封装类
     */
    public static <T> ResponseEntity<T> exchange(String url, HttpMethod method, HttpEntity<?> requestEntity, Class<T> responseType, Object... uriVariables) {
        return TEMPLATE.exchange(url, method, requestEntity, responseType, uriVariables);
    }

    /**
     * 通用调用方式
     * 
     * @param url 请求URL
     * @param method 请求方法类型
     * @param requestEntity 请求头和请求体封装对象
     * @param responseType 返回对象类型
     * @param uriVariables URL中的变量，与Map中的key对应
     * @return ResponseEntity 响应对象封装类
     */
    public static <T> ResponseEntity<T> exchange(String url, HttpMethod method, HttpEntity<?> requestEntity, Class<T> responseType, Map<String, ?> uriVariables) {
        return TEMPLATE.exchange(url, method, requestEntity, responseType, uriVariables);
    }

    /**
     * 获取RestTemplate实例对象，可自由调用其方法
     * 
     * @return RestTemplate实例对象
     */
    public static RestTemplate getRestTemplate() {
        return TEMPLATE;
    }

}
