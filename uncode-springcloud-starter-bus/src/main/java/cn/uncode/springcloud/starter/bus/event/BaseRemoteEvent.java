package cn.uncode.springcloud.starter.bus.event;


import org.springframework.cloud.bus.event.RemoteApplicationEvent;
import org.springframework.context.ApplicationEventPublisher;

import cn.uncode.springcloud.utils.obj.SpringUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * <一句话功能简述> <功能详细描述>
 *
 * @see: [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
@Slf4j
public class BaseRemoteEvent<T> extends RemoteApplicationEvent {

    /**
	 * 
	 */
	private static final long serialVersionUID = 187180320877892048L;

	public BaseRemoteEvent(T source) {
        super(source, SpringUtil.getContext().getId());

    }

    public BaseRemoteEvent(T source, String destinationServiceId) {
        super(source, SpringUtil.getContext().getId(), destinationServiceId + ":**");
    }

    public void fire() {
    	ApplicationEventPublisher publisher = SpringUtil.getContext().getBean(ApplicationEventPublisher.class);
        if (null != publisher) {
        	publisher.publishEvent(this);
            log.info("发布远程事件：{}", this);
        }
    }
}
